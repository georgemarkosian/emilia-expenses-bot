module.exports = class UserRecord {
    constructor(amount, currency, target, date) {
        this._amount = amount;
        this._currency = currency;
        this._target = target;
        this._date = date;
    }
    
    get amount()    { return this._amount; }
    get currency()  { return this._currency; }
    get target()    { return this._target; }
    get date()      { return this._date; }
    
    toDto() {
        return {
            amount: this._amount,
            currency: this._currency,
            target: this._target,
            date: this._date
        };
    }

    toJson() {
        return JSON.stringify(this.toDto());
    }
    
    /*
     * Static.
     */
    
    static fromDtoArray(dtos) {
        if (!dtos || !Array.isArray(dtos))
            return null;
        let items = [];
        for (let i = 0; i < dtos.length; i++)
            items.push(this.fromDto(dtos[i]));
        return items;
    }
    
    static fromDto(dto) {
        return new UserRecord(dto['amount'], 
                              dto['currency'], 
                              dto['target'],
                              new Date(dto['date']));
    }
    
    static toDtos(records) {
        let dtos = [];
        for (let i = 0; i < records.length; i++)
            dtos.push(records[i].toDto());
        return dtos;
    }
};