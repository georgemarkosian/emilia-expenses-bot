const UserRecord = require('./UserRecord');

module.exports = class UserRecords {
    constructor(userId, records) {
        this._userId = userId;
        this._records = records;
    }
    
    get userId() { return this._userId; }
    get records() { return this._records.concat(); }
    
    /*
     * Records.
     */
    
    addRecord(record) {
        if (record instanceof UserRecord) {
            this._records.push(record);    
            return;
        }
        throw new Error('Parameter is not an instance of UserRecord class.');
    }
    
    clearRecords() {
        this._records = [];
    }
    
    /*
     * Serialization.
     */

    toDto() {
        return {
            user_id: this._userId,
            records: UserRecord.toDtos(this.records)
        };
    }

    toJson() {
        return JSON.stringify(this.toDto());
    }
    
    /*
     * Static.
     */
    
    static fromUserId(userId) { 
        return new UserRecords(userId, []);
    }
    
    static fromDto(dto) {
        let userId = dto['user_id'];
        if (!userId)
            throw new Error('Invalid [user_id] parameter.');
        let records = UserRecord.fromDtoArray(dto['records']);
        if (records == null)
            throw new Error('Invalid [records] parameter.');
        return new UserRecords(userId, records);
    }
};