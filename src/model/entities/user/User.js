module.exports = class User {
	constructor(id, first_name, last_name, username, is_bot, is_admin, language_code, defaultGroupId, groupIds) {
		this._id 			 = id;
    	this._firstName 	 = first_name;
    	this._lastName 		 = last_name;
    	this._userName 		 = username;
    	this._isBot 		 = is_bot;
    	this._isAdmin		 = is_admin;
    	this._languageCode 	 = language_code;
    	this._defaultGroupId = defaultGroupId;
    	this._groupIds		 = groupIds;
	}

	get id() 		{ return this._id; }
	get name() 		{ return this._firstName; }
	get firstName()	{ return this._firstName; }
	get lastName()	{ return this._lastName; }
	get fullName() 	{ return this._firstName + ' ' + this._lastName; }
	get userName()	{ return this._userName; }
	get isBot()		{ return this._isBot; }

	get isAdmin() 	{ return this._isAdmin; }
	set isAdmin(value) {
		this._isAdmin = value;
	}
	
	get languageCode() 		{ return this._languageCode; }
	get defaultGroupId() 	{ return this._defaultGroupId; }
	get groupIds()			{ return this._groupIds; }

	toDto() {
		return {
			id: this._id,
			first_name: this._firstName,
			last_name: this._lastName,
			user_name: this._userName,
			is_bot: this._isBot,
			is_admin: this._isAdmin,
			language_code: this._languageCode,
			default_group_id: this._defaultGroupId,
			groupIds: this._groupIds
		};
	}

	toJson() {
		return JSON.stringify(this.toDto());
	}

	/*
	 * Static.
	 */

	static fromDto(dto) {
		return new User(dto.id, 
						dto.first_name, 
						dto.last_name, 
						dto.user_name, 
						dto.is_bot, 
						dto.is_admin, 
						dto.language_code, 
						dto.default_group_id, 
						dto.groupIds);
	}
};