const User 	 		= require('./entities/user/User');
const UserRecord 	= require('./entities/userRecords/UserRecord');
const UserRecords 	= require('./entities/userRecords/UserRecords');
const Response 		= require('./Response');

module.exports = class Model {
	constructor(repository) {
		this._repository = repository;
	}

	/*
	 * User.
	 */

	registerUser(id, first_name, last_name, username, is_bot, is_admin, language_code, callback) {
		this.getUser(id, result => {
			if (result.error != null) {
				callback(result);
				return;
			}
			if (result.success) {
				callback(Response.fromSuccess(false));
				return;
			}
			let user = new User(id, first_name, last_name, username, is_bot, is_admin, language_code, null, []);
			this._repository.user.create(user, function(result) {
				callback(result);
			});
		});
	}

	getUser(userId, callback) {
		this._repository.user.read(userId, callback);
	}

	deleteUser(userId, callback) {
		this.getUser(userId, (result) => {
			if (!result.success) {
				callback(result);
				return;
			}
			this._repository.user.delete(userId, result => {
				callback(result);
			});
		});
	}

	/*
	 * Records.
	 */

	getUserRecords(userId, callback) {
		this._repository.userRecords.read(userId, result => {
			if (result.error != null)
				return callback(result);
			
			// User has records file. Continue.
			if (result.success)
				return callback(result);

			// Creating empty records file.
			let userRecords = UserRecords.fromUserId(userId);
			this._repository.userRecords.create(userRecords, result => {
				return callback(result);
			});
		});
	}

	addRecord(user, amount, currency, target, date, callback) {
		this.getUserRecords(user.id, result => {
			if (result.error != null)
				return callback(result);
			let record = new UserRecord(amount, currency, target, date);
			let records = result.data;
			records.addRecord(record);
			this._repository.userRecords.update(records, result => {
				return callback(result);
			});
		});
	}
	
	clearUserRecords(userId, callback) {
		this.getUserRecords(userId, result => {
			if (result.error != null)
				return callback(result);
			let records = result.data;
			records.clearRecords();
			this._repository.userRecords.update(records, result => {
				if (result.error != null)
					return callback(result);
				return callback(result);
			});
		});
	}
};