module.exports = class Response  {
	constructor(data, error, success, message) {
		this._data = data;
		this._error = error;
		this._success = success;
		this._message = message;
	}

	get data() { return this._data; }
	get error() { return this._error; }
	get success() { return this._success === true; }
	get message() { return this._message; }
	
	toDto() {
		return {
			data: this.data,
			error: this.error,
			success: this.success,
			message: this.message
		};
	}

	toJson() {
		return JSON.stringify(this.toDto());
	}

	/*
	 * Static.
	 */

	static fromData(data, success) { 
		if (typeof success === 'undefined')
			return new Response(data, null, data != null); 
		return new Response(data, null, success === true); 
	}

	static fromError(error) { 
		return new Response(null, error, false); 
	}

	static fromSuccess(success, message) { 
		return new Response(null, null, success, message); 
	}
};