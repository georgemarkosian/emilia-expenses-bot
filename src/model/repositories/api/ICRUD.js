module.exports = class ICRUD extends require('../../../core/Interface') {
    validate() {
        this.checkMethodExists(this, 'create');
        this.checkMethodExists(this, 'read');
        this.checkMethodExists(this, 'update');
        this.checkMethodExists(this, 'delete');
    }
};