const fs = require('fs');
const UserRecords = require('../../entities/userRecords/UserRecords');
const Response = require('../../Response');

module.exports = class Records extends require('../api/ICRUD') {
	constructor(userRecordsPath) {
		super();
		this._userRecordsPath = userRecordsPath;
	}

	create(userRecords, callback) {
		let path = getUserRecordsFilePath(userRecords.userId, this._userRecordsPath);
		if (fs.existsSync(path))
			return callback(Response.fromSuccess(false, 'userRecords.create: User file already exists.'));
		let content = userRecords.toJson();
		fs.writeFile(path, content, error => {
			if (error != null)
				return callback(Response.fromError(error));
			return callback(Response.fromData(userRecords));
		});
	}

	read(userId, callback) {
		let path = getUserRecordsFilePath(userId, this._userRecordsPath);
		if (!fs.existsSync(path))
			return callback(Response.fromSuccess(false, 'userRecords.read: User\'s file not found.'));
		fs.readFile(path, (error, data) => {
			if (error)
				return callback(Response.fromError(error));
			
			let json = fixWeirdEndings(data.toString());
			let userRecordsDto = JSON.parse(json);
			let userRecords = UserRecords.fromDto(userRecordsDto);
			return callback(Response.fromData(userRecords));
		});
	}

	update(userRecords, callback) {
		let path = getUserRecordsFilePath(userRecords.userId, this._userRecordsPath);
		if (!fs.existsSync(path))
			return callback(Response.fromSuccess(false, 'userRecords.update: User\'s file not found.'));
		let content = fixWeirdEndings(userRecords.toJson());
		fs.writeFile(path, content, error => {
			if (error != null)
				return callback(Response.fromError(error));
			return callback(Response.fromData(userRecords));
		});
	}

	delete(userId, callback) {
		let path = getUserRecordsFilePath(userId, this._userRecordsPath);
		if (!fs.existsSync(path))
			return callback(Response.fromSuccess(false, 'userRecords.delete: User\'s file not found.'));
		fs.unlink(path, error => {
			if (error != null)
				return callback(Response.fromError(error));
			return callback(Response.fromSuccess(true));
		});
	}
};

/*
 * Helpers.
 */

function getUserRecordsFilePath(userId, recordsPath) {
	return recordsPath + '/' + userId;
}

function fixWeirdEndings(json)
{
	json = replaceEndingWith(json, '"}]}"}]}', '"}]}');
	json = replaceEndingWith(json, '}]}]}', '}]}');
	return json;
}

function replaceEndingWith(json, ending, newEnding)
{
	if (json.endsWith(ending))
	{
		let index = json.lastIndexOf(ending);
		json = json.slice(0, index) + json.slice(index).replace(ending, newEnding);
	}
	return json;
}