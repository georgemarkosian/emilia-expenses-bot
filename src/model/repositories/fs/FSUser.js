const fs = require('fs');
const User = require('../../entities/user/User');
const Response = require('../../Response');

module.exports = class Users extends require('../api/ICRUD') {
	constructor(usersPath) {
		super();
		this._usersPath = usersPath;
	}

	create(user, callback) {
		let path = getUserFilePath(user.id, this._usersPath);
		if (fs.existsSync(path))
			return callback(Response.fromSuccess(false, 'user.read: User\'s file already created.'));
		let content = user.toJson();
		fs.writeFile(path, content, error => {
			if (error != null)
				return callback(Response.fromError(error));
			return callback(Response.fromData(user));
		});
	}

	read(userId, callback) {
		let path = getUserFilePath(userId, this._usersPath);
		if (!fs.existsSync(path))
			return callback(Response.fromSuccess(false, 'user.read: User\'s file not found.'));
		fs.readFile(path, (error, data) => {
			if (error)
				return callback(Response.fromError(error));
			let userDto = JSON.parse(data.toString());
			let user = User.fromDto(userDto);
			return callback(Response.fromData(user));
		});
	}

	update(user, callback) {
		let path = getUserFilePath(user.id, this._usersPath);
		if (!fs.existsSync(path))
			return callback(Response.fromSuccess(false, 'user.update: User\'s file not found.'));
		let content = user.toJson();
		fs.writeFile(path, content, error => {
			if (error != null)
				return callback(Response.fromError(error));
			return callback(Response.fromData(user));
		});
	}

	delete(userId, callback) {
		let path = getUserFilePath(userId, this._usersPath);
		if (!fs.existsSync(path))
			return callback(Response.fromSuccess(false, 'user.delete: User\'s file not found.'));
		fs.unlink(path, error => {
			if (error != null)
				return callback(Response.fromError(error));
			return callback(Response.fromSuccess(true));
		});
	}
};

/*
 * Helpers.
 */

function getUserFilePath(userId, usersPath) {
	return usersPath + '/' + userId;
}