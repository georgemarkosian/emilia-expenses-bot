const fs 	 = require('fs');
const User 	 = require('./FSUser');
const Record = require('./FSUserRecords');

module.exports = class FS extends require('../IRepository') {
	inject(environmentUtil) {
		let rootPath = environmentUtil.getVariable('FILE_STORAGE_ROOT');
		if (!rootPath)
			throw new Error('Root path must be not null and not empty.');

		let usersPath = rootPath + '/users';
		let userRecordsPath = rootPath + '/records';

		createFolderIfDoesntExist(rootPath);
		createFolderIfDoesntExist(usersPath);
		createFolderIfDoesntExist(userRecordsPath);

		this._user = new User(usersPath);
		this._userRecords = new Record(userRecordsPath);
	}
	
	get user() 			{ return this._user; }
	get userRecords() 	{ return this._userRecords; }
};

function createFolderIfDoesntExist(path) {
	if (fs.existsSync(path))
		return false;
	fs.mkdirSync(path);
	console.log('Directory created: ' + path);
	return true;
}