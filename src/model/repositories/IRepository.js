module.exports = class IRepository extends require('../../core/Interface') {
    validate() {
        this.checkMethodExists(this, 'user');
        this.checkMethodExists(this, 'userRecords');
    }
};