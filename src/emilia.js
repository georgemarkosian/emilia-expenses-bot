const Telegraf 		    	= require('telegraf');
const commandParts      	= require('telegraf-command-parts');
const DependencyBinder 		= require('./ioc/DependencyBinder');
const CommandResultAction 	= require('./commands/CommandResultAction');

console.log('Launching Emilia...');

let binder = new DependencyBinder('dependencyBinder');
binder.addDependency('environmentUtil', 			require('./utils/EnvironmentUtil'));
binder.addDependency('functionUtil', 				require('./utils/FunctionUtil'));
binder.addDependency('stringUtil', 					require('./utils/StringUtil'));
binder.addDependency('dateUtil', 					require('./utils/DateUtil'));
binder.addDependency('arrayUtil', 					require('./utils/ArrayUtil'));
binder.addDependency('commandProcessor', 			require('./commands/CommandProcessor'));
binder.addDependency('repository', 					require('./model/repositories/fs/FS'));
binder.addDependency('model', 						require('./model/Model'));
binder.addDependency('localization', 				require('./localization/localization'));
binder.addDependency('currencyManager', 			require('./common/currency/CurrencyManager'));
binder.addDependency('logManager', 					require('./common/LogManager'));
binder.addDependency('unprocessedMessagesManager', 	require('./common/UnprocessedMessagesManager'));
binder.bind((commandProcessor, environmentUtil) => {

	// Request commands.
//	commandProcessor.registerRequestCommand(require('./commands/request/LogMessageCmd'));
	commandProcessor.registerRequestCommand(require('./commands/request/StartCmd'));
	commandProcessor.registerRequestCommand(require('./commands/request/CheckUserPermittedCmd'));
	commandProcessor.registerRequestCommand(require('./commands/request/CheckUserRegisteredCmd'));
	commandProcessor.registerRequestCommand(require('./commands/request/HelpCmd'));
	commandProcessor.registerRequestCommand(require('./commands/request/ShowRecords'));
	commandProcessor.registerRequestCommand(require('./commands/request/CalculateCommonExpensesCmd'));
	commandProcessor.registerRequestCommand(require('./commands/request/MakeRecordCmd'));
	commandProcessor.registerRequestCommand(require('./commands/request/SmallTalkCmd'));
	commandProcessor.registerRequestCommand(require('./commands/request/ClearRecordsCmd'));
	commandProcessor.registerRequestCommand(require('./commands/request/NotSoSmartCmd'));

	// Response commands.
	commandProcessor.registerActionCommand(CommandResultAction.REPLY_WITH_STRING, 	require('./commands/response/ReplyWithStringCmd'));
	commandProcessor.registerActionCommand(CommandResultAction.NONE, 				require('./commands/response/DoNothingCmd'));
	
	// Bot startup.
	const bot = new Telegraf(environmentUtil.getVariable('BOT_TOKEN'));
	bot.use(commandParts());
	bot.on('text', (context) => {
		commandProcessor.processRequest(context);	
	});
	bot.catch(error => {
		//console.log(error instanceof Error); // true
		console.error(error);
	});
	bot.startPolling();

	console.log("Emilia is online.");
});