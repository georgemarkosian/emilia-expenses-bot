module.exports = class ArrayUtil {

    haveSameItem(array1, array2) {
        for (let i = 0; i < array1.length; i++) {
            for (let j = 0; j < array2.length; j++) {
                if (array1[i] === array2[j])
                    return true;
            }
        }
        return false;
    }
    
    startsWithAny(string, arrayOfString) {
        for (let i = 0; i < arrayOfString.length; i++) {
            if (string.startsWith(arrayOfString[i]))
                return true;
        }
        return false;
    }
    
    getRandomItem(array) {
        return array[Math.floor(Math.random() * array.length)]
    }

    getRandomItems(array, number) {
        let result = new Array(number);
        let len = array.length;
        let taken = new Array(len);
        if (number > len)
            throw new RangeError("getRandom: more elements taken than available");
        while (number--) {
            let x = Math.floor(Math.random() * len);
            result[number] = array[x in taken ? taken[x] : x];
            taken[x] = --len in taken ? taken[len] : len;
        }
        return result;
    }
};