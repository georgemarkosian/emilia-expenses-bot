const dateFormat = require('dateformat');

module.exports = class DateUtil {
    constructor(localization) {
        this._localization = localization;
    }
    
    /*
     * Is.
     */
    
    isToday(date) {
        const today = new Date();
        return  date.getDate() === today.getDate() &&
                date.getMonth() === today.getMonth() &&
                date.getFullYear() === today.getFullYear();
    }
    
    isYesterday(date) {
        let yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        return  date.getDate() === yesterday.getDate() &&
                date.getMonth() === yesterday.getMonth() &&
                date.getFullYear() === yesterday.getFullYear();
    }
    
    /*
     * Normalization.
     */
    
    toRawDate(date) {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }
    
    /*
     * Format.
     */

    formatTime(date) { return dateFormat(date, this._localization.getString(null, 'format_time')); }
    formatDate(date) { return dateFormat(date, this._localization.getString(null, 'format_date')); }
    formatDateTime(date, pattern) {
        return dateFormat(date, pattern);
    }
};