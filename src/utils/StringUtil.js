module.exports = class StringUtil {
    
    format(string, ...params) {
        for (let k in params)
            string = string.replace("{" + k + "}", params[k]);
        return string;
    }
    
    removePunctuation(string) {
        return string.replace(/[.,\/#!%\^&\*;:{}=\-_`~()]/g,"");
    };
    
};