const fs = require('fs');

module.exports = class Localization {
	constructor(arrayUtil, environmentUtil, stringUtil) {
		this._arrayUtil = arrayUtil;
		this._stringUtil = stringUtil;
		this._defaultLocale = environmentUtil.getVariable('DEFAULT_LOCALE');
		this._locales = readLocales(environmentUtil.getVariable('LOCALES_FOLDER'));
	}
	
	/*
	 * Public.
	 */

	getLocale(code) {
		let locale = this._locales[code];
		if (!locale)
			return this._locales[this._defaultLocale];
		if (!locale)
			throw new Error('Default locale not found. [' + this._defaultLocale + ']');
		return locale;
	}
	
	getString(code, key, ...params) {
		let locale = this.getLocale(code);
		let string = locale.strings[key];
		if (string == null)
			return key;
		if (string instanceof Array)
			string = string.length > 0 ? this._arrayUtil.getRandomItem(string) : key;
		if (params && params.length > 0)
			string = this.formatString(string, params);
		return string;
	};
	
	getStrings(code, key, ...params) {
		let locale = this.getLocale(code);
		let strings = locale.strings[key];
		if (strings == null)
			return [key];
		if (!params || params.length === 0)
			return strings;
		if (strings instanceof Array) {
			for (let i = 0; i < strings.length; i++)
				strings[i] = this.formatString(strings[i], params)
		} else {
			strings = this.formatString(strings, params);
		}
		return strings;
	}

	/*
	 * Helpers.
	 */
	
	formatString(string, params) {
		return this._stringUtil.format.apply(this._stringUtil, [string, params]);
	}
};

/*
 * Helpers.
 */

function readLocales(localesFolderPath) {
	let locales = {};
	let filesNames = fs.readdirSync(localesFolderPath);
	filesNames.forEach(item => {
		let fileContent = fs.readFileSync(localesFolderPath + '/' + item).toString();
		let fileContentObject = JSON.parse(fileContent);
		locales[fileContentObject.code] = fileContentObject;
	});
	return locales;
}