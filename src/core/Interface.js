module.exports = class Interface {
    constructor() {
        this.validate();
    }

    /*
     * Protected to be overridden.
     */
    
    validate() {
        throw new Error('Not implemented.');
    }

    /*
     * Protected.
     */
    
    checkMethodExists(owner, methodName) {
        let descriptor = Object.getPrototypeOf(owner);
        if (!descriptor.hasOwnProperty(methodName))
            throw new Error('Method not defined: [' + methodName + ']');
    }
};