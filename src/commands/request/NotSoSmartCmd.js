module.exports = class NotSoSmartCmd extends require('../CommandBase') {
    execute(params, unprocessedMessagesManager) {
        unprocessedMessagesManager.addMessage(params.message);
        return this.replyWithString('im_not_so_smart_yet');
    }
};