module.exports = class SmallTalkCmd extends require('../CommandBase') {
    execute(params, localization, arrayUtil) {
        this._params = params;
        this._localization = localization;
        this._arrayUtil = arrayUtil;
        
        let message = this._params.messageLowered;
        let hi = this._localization.getStrings(this._params.languageCode, 'hi');
        if (message.startsWith(hi))
            return this.replyWithString('hello');

        let thanks = this._localization.getStrings(this._params.languageCode, 'thanks');
        if (this._arrayUtil.startsWithAny(message, thanks))
            return this.replyWithString('you_are_welcome');

        let pirozhki = this._localization.getStrings(this._params.languageCode, 'pirozhki');
        if (this._arrayUtil.startsWithAny(message, pirozhki))
            return this.replyWithString('pirozhochki');
        
        this.proceed();
    }
};