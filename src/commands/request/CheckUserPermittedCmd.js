module.exports = class CheckUserPermittedCmd extends require('../CommandBase') {
    execute(params, model) {
        let userId = params.userId;
        let userIdAlina = 375790666;
        let userIdGeorge = 382847529;
        if (userId === userIdAlina || userId === userIdGeorge)
            return this.proceed();
        this.replyWithString('message_user_not_permitted');
    }
};