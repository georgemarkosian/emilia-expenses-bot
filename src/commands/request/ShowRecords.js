module.exports = class ShowMyRecordsCmd extends require('../CommandBase') {
    execute(params, model, localization, currencyManager, dateUtil, arrayUtil) {
        this._params = params;
        this._localization = localization;
        this._currencyManager = currencyManager;
        this._dateUtil = dateUtil;
        this._arrayUtil = arrayUtil;
        
        if (!this.validate())
            return this.proceed();
        
        let targeting = this.getTargeting(params);
        let timeSpan = this.getTimeSpan(params);
        
        model.getUserRecords(params.userId, result => {
            if (!result.success)
                return this.error();
            
		    let records = result.data.records;
		    if (records.length === 0)
                return this.replyWithString('you_have_no_records');

		    let recordsGrouped = this.groupRecords(records);
            let keys = Object.keys(recordsGrouped);
            
            let key;
            let message = '';
            let sum = 0;
            let record;
            for (let i = 0; i < keys.length; i++) {
                if (i > 0)
                    message += '\n';
                key = keys[i]; 
                message += '*' + this.formatDate(new Date(key)) + ':*\n';
                records = recordsGrouped[key];
                for (let j = 0; j < records.length; j++)
                {
                    record = records[j];
                    message += this.formatRecord(record, params).toString() + '\n';
                    sum += record.amount;
                }
            }
            
            message += '\n*' + this._localization.getString(this._params.languageCode, 'Total') + ':* ';
            message += sum + ' ' + this.formatCurrency(record) + '\n';
            
            return this.replyWithString(message);
	    });
    }
    
    validate() {
        let commandStrings  = this._params.messageLowered.split(' ');
        let myStrings       = this._localization.getStrings(this._params.languageCode, 'my');
        let showStrings     = this._localization.getStrings(this._params.languageCode, 'show');
        let expensesStrings = this._localization.getStrings(this._params.languageCode, 'expenses');
        
        // Check has 'expenses' word.
        if (!this._arrayUtil.haveSameItem(commandStrings, expensesStrings))
            return false;
        
        // Check has 'my' or 'our' or 'show' words.
        return  this._arrayUtil.haveSameItem(commandStrings, myStrings) ||
                this._arrayUtil.haveSameItem(commandStrings, showStrings);
    }
    
    getTargeting(params) {
        return null;
    }
    
    getTimeSpan(params) {
        return null;
    }
    
    groupRecords(records) {
        let res = {};
        let record;
        let date;
        for (let i = 0; i < records.length; i++) {
            record = records[i];
            date = this._dateUtil.toRawDate(record.date);
            if (date in res) {
                res[date].push(record);
            } else {
                res[date] = [record];
            }
        }
        return res;
    }
    
    formatDate(date) {
        if (this._dateUtil.isToday(date))
            return this._localization.getString(this._params.languageCode, 'Today');
        if (this._dateUtil.isYesterday(date))
            return this._localization.getString(this._params.languageCode, 'Yesterday');
        return this._dateUtil.formatDate(date);
    }
    
    formatRecord(record) {
        let string = record.amount + ' ' + this.formatCurrency(record);
        if (record.target)
            string += ' ' + record.target;
        return string;
    }
    
    formatCurrency(record) {
        let currency = this._currencyManager.getByKey(record.currency);
        if (currency == null)
            currency = this._currencyManager.getByString(record.currency, this._params.languageCode);
        return (currency ? this._localization.getString(this._params.languageCode, currency.key + '_name') : '???');
    }
};