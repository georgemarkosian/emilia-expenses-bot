module.exports = class ClearRecordsCmd extends require('../CommandBase') {
    execute(params, model, localization) {
        this._params = params;
        this._model = model;
        this._localization = localization;
        
        if (!this.validate())
            return this.proceed();
        
        model.getUser(params.userId, result => {
            if (!result.success)
                return this.error();
            
            let user = result.data;
            if (!user.isAdmin)
                return this.replyWithString('you_are_not_permitted');

            let userIdAlina = 375790666;
            let userIdGeorge = 382847529;
            
            this._model.clearUserRecords(userIdAlina, result => {
                if (!result.success)
                    return this.error();
                this._model.clearUserRecords(userIdGeorge, result => {
                    if (!result.success)
                        return this.error();
                    this.replyWithString('clear_records_done');
                });
            });
        });
    }

    validate() {
        let commandString  = this._params.messageLowered;
        let clearRecordsStrings = this._localization.getStrings(this._params.languageCode, 'clear_records');
        return commandString === clearRecordsStrings;
    }
};