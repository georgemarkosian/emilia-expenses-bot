module.exports = class HelpCmd extends require('../CommandBase') {
    execute(params) {
        if (params.messageLowered !== '/help')
            return this.proceed();
        return this.replyWithString('help');
    }
};