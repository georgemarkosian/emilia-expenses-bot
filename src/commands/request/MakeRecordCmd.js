module.exports = class MakeRecordCmd extends require('../CommandBase') {
    execute(params, model, localization, currencyManager, arrayUtil, stringUtil) {
        this._params = params;
        this._model = model;
        this._localization = localization;
        this._currencyManager = currencyManager;
        this._arrayUtil = arrayUtil;
        this._stringUtil = stringUtil;

        // implement bulk messages recording
        
        let messages = params.messageLowered.split('\n');
        this.processMessage(messages[0]);   
    }
    
    processMessage(message) {
        let commandStrings  = message.split(' ');
        let recordStrings   = this._localization.getStrings(this._params.languageCode, 'record_make');

        let startsWithRecordWordAndAmount = recordStrings.includes(commandStrings[0]) && this.isAmount(commandStrings[1]);
        let startsWithAmount = this.isAmount(commandStrings[0]);
        if (!startsWithRecordWordAndAmount && !startsWithAmount)
            return this.proceed();

        let amount;
        let amountIndex;
        if (this.isAmount(commandStrings[0])) {
            amount = parseInt(commandStrings[0]);
            amountIndex = 0;
        } else {
            amount = parseInt(commandStrings[1]);
            amountIndex = 1;
        }

        if (!this.hasCurrency(commandStrings, amountIndex))
            return this.replyWithString('record_make_no_currency', this.getCurrenciesExamplesString());

        let currency = this.getCurrency(commandStrings, amountIndex);
        if (currency == null)
            return this.replyWithString('record_make_invalid_currency', this.getSupportedCurrenciesString());

        let target = this.getTarget(commandStrings, amountIndex);

        this._model.getUser(this._params.userId, result => {
            if (!result.success)
                return this.replyWithString('error');
            let user = result.data;
            this._model.addRecord(user, amount, currency.key, target, new Date(), result => {
                if (!result.success)
                    return this.replyWithString('error');
                return this.replyWithString('message_record_success');
            });
        });
    }

    /*
     * Amount.
     */
    
    isAmount(string) {
        let amount = parseInt(string);
        if (!amount)
            return false;
        return true;
    }

    /*
     * Currency.
     */
    
    hasCurrency(commandStrings, amountIndex) {
        return commandStrings.length > amountIndex + 1;
    }
    
    getCurrency(commandStrings, amountIndex) {
        let currencyString = commandStrings[amountIndex + 1];
        currencyString = this._stringUtil.removePunctuation(currencyString);
        return this._currencyManager.getByString(currencyString, this._params.languageCode);
    }
    
    getCurrenciesExamplesString() {
        let res = [];
        this._currencyManager.ALL.forEach(currency => {
            res = res.concat(this._localization.getStrings(this._params.languageCode, currency.key + '_names'));
        });
        res = this._arrayUtil.getRandomItems(res, 5);
        return res.join(', ');
    }
    
    getSupportedCurrenciesString() {
        return this._localization.getString(this._params.languageCode, 'supported_currencies_string');
    }
    
    /*
     * Target.
     */
    
    getTarget(commandStrings, amountIndex) {
        if (commandStrings.length > amountIndex + 2)
            return commandStrings[amountIndex + 2];
        return null;
    }
};