module.exports = class CalculateCommonExpensesCmd extends require('../CommandBase') {
    execute(params, model, localization, currencyManager, dateUtil, arrayUtil) {
        this._params = params;
        this._localization = localization;
        this._currencyManager = currencyManager;
        this._dateUtil = dateUtil;
        this._arrayUtil = arrayUtil;

        if (!this.validate())
            return this.proceed();

        let userIdAlina = 375790666;
        let userIdGeorge = 382847529;
        
        model.getUserRecords(userIdAlina, resultAlina => {
            if (!resultAlina.success)
                return this.error();
            
            model.getUserRecords(userIdGeorge, resultGeorge => {
                if (!resultGeorge.success)
                    return this.error();

                let userRecordsAlina = resultAlina.data.records;
                let userRecordsGeorge = resultGeorge.data.records;
                
                if (userRecordsAlina.length === 0 && userRecordsGeorge.length === 0)
                    return this.replyWithString('you_both_have_no_records');
                
                let sumAlina = this.sumRecordsAmounts(userRecordsAlina);
                let sumGeorge = this.sumRecordsAmounts(userRecordsGeorge);
                let sum = sumAlina + sumGeorge;
                let sumPerPerson = sum / 2;
                
                let currencyString = this.formatCurrency(userRecordsAlina.length !== 0 ? userRecordsAlina[0] : userRecordsGeorge[0]);
                let message = '';
                message += '*Alina:*\n' + sumAlina + ' ' + currencyString + '\n\n';
                message += '*George:*\n' + sumGeorge + ' ' + currencyString + '\n\n';
                message += '*' + this._localization.getString(this._params.languageCode, 'Total') + ':*\n' + sum + ' ' + currencyString + '\n\n';
                message += '*' + this._localization.getString(this._params.languageCode, 'Per_person') + ':*\n' + sumPerPerson + ' ' + currencyString + '\n\n';
                
                let debtAlina = this.calculateDebt(sumAlina, sumPerPerson);
                if (debtAlina !== 0)
                    message += '*Alina:*\n' + this.calculateDebt(sumAlina, sumPerPerson) + ' ' + currencyString + '\n\n';

                let debtGeorge = this.calculateDebt(sumGeorge, sumPerPerson);
                if (debtGeorge !== 0)
                    message += '*George:*\n' + this.calculateDebt(sumGeorge, sumPerPerson) + ' ' + currencyString + '\n\n';

                return this.replyWithString(message);
            });
        });
    }

    validate() {
        let commandStrings  = this._params.messageLowered.split(' ');
        let ourStrings      = this._localization.getStrings(this._params.languageCode, 'our');
        let showStrings     = this._localization.getStrings(this._params.languageCode, 'show');
        let expensesStrings = this._localization.getStrings(this._params.languageCode, 'expenses');

        // Check has 'expenses' word.
        if (!this._arrayUtil.haveSameItem(commandStrings, expensesStrings))
            return false;

        // Check has 'my' or 'our' or 'show' words.
        return  this._arrayUtil.haveSameItem(commandStrings, ourStrings) ||
                this._arrayUtil.haveSameItem(commandStrings, showStrings);
    }

    sumRecordsAmounts(records) {
        let sum = 0;
        for (let i = 0; i < records.length; i++)
            sum += records[i].amount;
        return sum;
    }

    calculateDebt(expenses, expensesPerPerson) {
        if (expenses >= expensesPerPerson)
            return 0;
        return expenses - expensesPerPerson;
    }
    
    formatCurrency(record) {
        let currency = this._currencyManager.getByKey(record.currency);
        if (currency == null)
            currency = this._currencyManager.getByString(record.currency, this._params.languageCode);
        return (currency ? this._localization.getString(this._params.languageCode, currency.key + '_name') : '???');
    }
};