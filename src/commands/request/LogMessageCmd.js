module.exports = class LogMessageCmd extends require('../CommandBase') {
    execute(params, logManager) {
        let log = new Date() + ': ' + JSON.stringify(params.context) + '\n'; 
        logManager.log(log);
        return this.proceed();
    }
};