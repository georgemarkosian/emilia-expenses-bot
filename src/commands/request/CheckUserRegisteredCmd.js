module.exports = class CheckUserRegisteredCmd extends require('../CommandBase') {
    execute(params, model) {
        model.getUser(params.userId, result => {
            // User already registered. Proceed to the next command.
            if (result.success)
                return this.proceed();
            this.replyWithString('message_user_not_found');
        });
    }
};