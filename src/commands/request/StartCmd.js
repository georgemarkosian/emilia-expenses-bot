module.exports = class StartCmd extends require('../CommandBase') {
    execute(params, model, environmentUtil) {
        if (params.messageLowered !== '/start')
            return this.proceed();

        const context = params.context;
        const userDto = context.message.from;
        const handler = result => {
            
            // User successfully registered.
            if (result.success)
                return this.replyWithString('start');
            
            // Error during execution.
            if (result.error != null)
                return this.replyWithString('error');
            
            // No errors, but action isn't successful. User already registered. 
            return this.replyWithString('start_again');
        };
        
        model.registerUser(userDto.id,
                           userDto.first_name,
                           userDto.last_name,
                           userDto.username,
                           userDto.is_bot,
                           userDto.id === environmentUtil.getVariable('ADMIN_USER_ID'),
                           userDto.language_code, 
                           handler);
    }
};