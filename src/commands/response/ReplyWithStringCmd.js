module.exports = class ReplyWithStringCmd extends require('../CommandBase') {
    execute(result, context, localization) {
        let key = result.data[0];
        let params = result.data.slice(1, result.data.length);
        replyWithString.apply(null, [localization, context, key].concat(params));
    }
};

function replyWithString(localization, context, key, ...params) {
    let string = getString.apply(null, [localization, context, key].concat(params));
    return context.replyWithMarkdown(string);
}

function getString(localization, context, key, ...params) {
    let args = [context.message.from.language_code, key].concat(params);
    return localization.getString.apply(localization, args)
}