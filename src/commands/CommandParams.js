module.exports = class CommandParams {
    constructor(context) {
        this._context = context;
    }

    get context()           { return this._context; }
    get userId()            { return this._context.message.from.id; }
    get message()           { return this._context.message.text; }
    get messageLowered()    { return this._context.message.text.toLowerCase(); }
    get languageCode()      { return this._context.message.from.language_code; }
};