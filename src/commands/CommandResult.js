module.exports = class CommandResult {
    constructor(action, data, proceed) {
        this._action = action;
        this._data = data;
        this._proceed = proceed === true;
    }
    get action()    { return this._action; }
    get data()      { return this._data; }
    get proceed()   { return this._proceed; }
};