const CommandResult = require('./CommandResult');
const CommandResultAction = require('./CommandResultAction');

module.exports = class CommandBase {
    constructor(callback) {
        this._callback = callback;
    }
    
    execute() {
        throw new Error('Not implemented.');
    }
    
    /*
     * Result.
     */

    respond(result) {
        if (!this._callback)
            throw new Error('Callback not specified.');
        setTimeout(this._callback, 1, result);
    }

    void() {
        this.respond(new CommandResult(CommandResultAction.NONE, null, false));
    }
    
    proceed() {
        this.respond(new CommandResult(CommandResultAction.NONE, null, true));
    }

    replyWithString(stringOrKey, ...params) {
        this.respond(new CommandResult(CommandResultAction.REPLY_WITH_STRING, [stringOrKey].concat(params)));
    }
    
    error() {
        this.replyWithString('error');
    }
};