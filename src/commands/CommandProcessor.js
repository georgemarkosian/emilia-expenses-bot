const CommandParams = require('./CommandParams');

module.exports = class CommandProcessor {
    constructor(dependencyBinder, functionUtil) {
        this._dependencyBinder          = dependencyBinder;
        this._functionUtil              = functionUtil;
        this._requestCommandPrototypes  = [];   // List of commands to follow by order.
        this._actionCommandPrototypes   = {};   // Arranged by command result action.
    }
    
    /*
     * Public.
     */
    
    registerRequestCommand(commandPrototype) {
        this._requestCommandPrototypes.push(commandPrototype);
    }
    
    registerActionCommand(action, commandPrototype) {
        this._actionCommandPrototypes[action] = commandPrototype;
    }
    
    processRequest(context) {
        console.log('================================================================================');
        this.processWithRequestCommand(0, context);    
    }
    
    /*
     * Private.
     */

    processWithRequestCommand(commandIndex, context) {
        let commandPrototype = this._requestCommandPrototypes[commandIndex];
        if (!commandPrototype)
            throw new Error('Command not found.');

        let handler = result => {

            console.log(commandPrototype.name + ': executed. Processing result...');

            let actionCommandPrototype = this._actionCommandPrototypes[result.action];
            if (actionCommandPrototype == null)
                throw new Error('Command not found for action: ' + result.action);

            this.executeCommandWithDependencies(new actionCommandPrototype(), context, result);
            console.log(commandPrototype.name + ': result processed');

            if (result.proceed) {
                console.log(commandPrototype.name + ': proceeding to the next command...');
                this.processWithRequestCommand(commandIndex + 1, context);
            }
        };

        console.log(commandPrototype.name + ': executing...');
        this.executeCommandWithDependencies(new commandPrototype(handler), context);
    }

    executeCommandWithDependencies(command, context, result) {
        let params = this.getMethodDependencies(command.execute, context, result);
        command.execute.apply(command, params);
    }

    getMethodDependencies(method, context, result) {
        let paramNames = this._functionUtil.getParamNames(method);
        if (paramNames.length === 0)
            return [];
        let params = [];
        paramNames.forEach(paramName => {
            let dependency = this._dependencyBinder.getDependency(paramName);
            if (dependency == null) {
                switch (paramName) {
                    case 'params':
                        dependency = new CommandParams(context);
                        break;
                    case 'context':
                        dependency = context;
                        break;
                    case 'result':
                        dependency = result;
                        break;
                }
                if (dependency == null)
                    throw new Error('Dependency not found. [' + paramName + ']');
            }
            params.push(dependency);
        });
        return params;
    }
};