const Currency = require('./Currency');

module.exports = class CurrencyManager {
    constructor(localization) {
        this._localization = localization;
        
        this._all = [];
        this._allByKey = {};
        this._allById = {};

        this._usd = this.add(new Currency(1, 'usd', '$'));
        this._eur = this.add(new Currency(2, 'eur', '€'));
        this._uah = this.add(new Currency(3, 'uah', '₴'));
        this._rub = this.add(new Currency(4, 'rub', '₽'));
    }
    
    add(currency) {
        this._all.push(currency);
        this._allById[currency.id] = currency;
        this._allByKey[currency.key] = currency;
        return currency;
    }

    get ALL() { return this._all;}
    get USD() { return this._usd; }
    get EUR() { return this._eur; }
    get UAH() { return this._uah; }
    get RUB() { return this._rub; }
    
    getById(key) { return this._allById[key]; }
    getByKey(key) { return this._allByKey[key]; }
    getByString(string, languageCode) {
        let currency;
        let currencyStrings;
        for (let i = 0; i < this._all.length; i++) {
            currency = this._all[i];
            currencyStrings = this._localization.getStrings(languageCode, currency.key + '_names');
            if (currencyStrings.includes(string))
                return currency;
        }
        return null;
    }
};