const fs = require('fs');

module.exports = class UnprocessedMessagesManager {
    constructor(environmentUtil) {
        this._filePath = environmentUtil.getVariable('UNPROCESSED_MESSAGES_FILE');

        // Creating file if it doesn't exist.
        if (!fs.existsSync(this._filePath))
            fs.writeFile(this._filePath, "", function(error) {});
    }
    
    addMessage(message) {
        fs.appendFile(this._filePath, message + '\r\n', function(error) {});
    }
};