const fs = require('fs');

module.exports = class LogManager {
    constructor(environmentUtil) {
        this._filePath = environmentUtil.getVariable('LOG_FILE');

        // Creating file if it doesn't exist.
        if (!fs.existsSync(this._filePath))
            fs.writeFile(this._filePath, "", function(error) {});
    }

    log(message) {
        fs.appendFile(this._filePath, message + '\r\n', function(error) {});
    }
};