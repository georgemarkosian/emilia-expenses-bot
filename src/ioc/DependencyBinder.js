module.exports = class DependencyBinder {
    constructor(dependencyName) {
        this._dependencyPrototypes  = {};                   // Dependency prototypes arranged by name.
        this._dependencyInstances   = {};                   // Dependency prototypes arranged by name.
        this._dependencyInstances[dependencyName] = this;   // Dependency binder will be injected by specified name.
    }
    
    /*
     * Public.
     */
    
    addDependency(name, prototype) {
        this._dependencyPrototypes[name] = prototype;
    }
    
    getDependency(name) {
        let dependency = this._dependencyInstances[name];
        if (dependency != null)
            return dependency;
        
        let dependencyPrototype = this._dependencyPrototypes[name];
        if (dependencyPrototype == null)
            return null;
        
        dependency = this.constructWithDependencies(dependencyPrototype);
        this.applyWithDependencies(dependency, 'inject');
        
        this._dependencyInstances[name] = dependency;
        console.log('Dependency instantiated: ' + name);
        
        return dependency;
    }
    
    bind(callback) {
        // This will instantiate added dependencies.
        // Actually this might be removed for lazy initialization.
        for (let key in this._dependencyPrototypes) {
            if (!this.getDependency(key))
                throw new Error('Dependency not found: ' + name);
        }
        
        // Call callback only if it was specified.
        if (callback === null)
            return;
        let argsNames = this.getArgumentsNames(callback);
        let args = this.getArguments(argsNames);
        callback.apply(this, args);
    }

    /*
     * Private.
     */

    constructWithDependencies(prototype) {
        if (prototype == null)
            return null;
        if (prototype.length === 0)
            return new prototype();
        let argsNames = this.getArgumentsNames(prototype);
        let args = this.getArguments(argsNames);
        return new prototype(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
    }
    
    applyWithDependencies(owner, methodName) {
        let method = owner[methodName];
        if (method == null)
            return;
        if (method.length === 0)
            return method();
        let argsNames = this.getArgumentsNames(method);
        let args = this.getArguments(argsNames);
        return method.apply(owner, args);
    }

    getArgumentsNames(func) {
        let fnStr = func.toString().replace(/((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg, '');
        let result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(/([^\s,]+)/g);
        if (result === null)
            result = [];
        return result;
    }

    getArguments(argsNames) {
        if (argsNames.length === 0)
            return [];
        let args = [];
        argsNames.forEach(argName => {
            args.push(this.getDependency(argName));
        });
        return args;
    }
};